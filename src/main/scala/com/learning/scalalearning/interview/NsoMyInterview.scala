package com.learning.scalalearning.interview

object NsoMyInterview extends App {
  val map = Map(
    1 -> List(2, 3, 4),
    2 -> List(3),
    4 -> List(3, 1, 8),
    5 -> List(6),
    10 -> List(7, 1),
    8 -> List(15)
  )
  def isConnected_0(from: Int, to: Int, g: Map[Int, List[Int]]):Boolean = {
    if (g(from).contains(to)) true
    else {
      var result = false
      for (i <- g(from)) {
        result = result || isConnected_0(i, to, g)
      }
      result
    }
  }
  def isConnected_1(from: Int, to: Int, g: Map[Int, List[Int]]):Boolean = {
    if (g.getOrElse(from, List()).contains(to)) true
    else {
      var result = false
      for (i <- g.getOrElse(from, List())) {
        result = result || isConnected_1(i, to, g)
      }
      result
    }
  }

  def isConnected_2(from: Int, to: Int, g: Map[Int, List[Int]]):Boolean = {
    if (g.getOrElse(from, List()).contains(to)) true
    else g.getOrElse(from, List()).exists(isConnected_2( _ ,to , g))
  }

  def isConnected_3(from: Int, to: Int, g: Map[Int, List[Int]], discovered: Set[Int]):Boolean = {
    if (discovered.contains(from)) false
    else if (g.getOrElse(from, List()).contains(to)) true
    else g.getOrElse(from, List()).exists(isConnected_3( _ ,to , g, discovered + from))
  }

  def isConnected_4(from: Int, to: Int, g: Map[Int, List[Int]])(discovered: Set[Int]):Boolean = {
    if (discovered.contains(from)) false
    else if (g.getOrElse(from, List()).contains(to)) true
    else g.getOrElse(from, List()).exists(isConnected_3( _ ,to , g, discovered + from))
  }

  val isConnected_cary = isConnected_4(_: Int, _ : Int, _: Map[Int, List[Int]])(Set())
  //println(isConnected_0(4, 2, map))
  println(isConnected_1(4, 2, map))
  println(isConnected_2(4, 2, map))
  println(isConnected_cary(4, 2, map))


  val h :: t = List(1, 2, 3)

  def head[T](list: List[T]): Option[T] = {
    list match {
      case List() => None
      case h :: _ => Some(h)
    }
  }

  def calAllPerfectNumbersUpTo(n: Int): List[Int] = {
    (1 to n).map(i =>{
      val divisors = (1 to i)
        .filter(d => i % d == 0)
      (i, divisors)
    }).filter(t => t._1 == t._2.reduce(_+_)/2)
      .map(_._1)
      .toList
  }

  println(calAllPerfectNumbersUpTo(10))

  def calAllPerfectNumbersUpTo_1(n: Int): List[Int] = {
    (1 to n).map(i =>{
      var d = 2
      var divisors: scala.collection.mutable.Set[Int] = scala.collection.mutable.Set(1)
      while(d*d <= i){
        if (i % d == 0)
          divisors += d += i/d//divisors :+= d//divisors = divisors :+ d
        d += 1
      }
      (i, divisors)
    }).filter(t => t._1 == t._2.reduce(_+_)/2)
      .map(_._1)
      .toList
  }
  println(calAllPerfectNumbersUpTo_1(10))

  def calAllPerfectNumbersUpTo_1_1(n: Int): List[Int] = {
    (1 to n)
      .filter(isPerfect_1_1)
      .toList
  }

  def isPerfect_1_1(n: Int): Boolean = {
    var d = 1
    var divisors: scala.collection.mutable.Set[Int] = scala.collection.mutable.Set(0)
    while(d*d <= n){
      if (n % d == 0)
        divisors += d += n/d//divisors :+= d//divisors = divisors :+ d
      d += 1
    }
    n == divisors.reduce(_+_).toInt/2
  }

  println(calAllPerfectNumbersUpTo_1_1(1000))

  def calAllPerfectNumbersUpTo_2(n: Int): List[Int] = {
    (1 to n)
      .filter(isPerfect)
      .toList
  }

  def isPerfect(n: Int): Boolean = {
    n == (1 to n / 2)
      .filter(m => n % m == 0)
      .reduceOption(_ + _)
      .getOrElse(0)
  }

  println(calAllPerfectNumbersUpTo_2(1000))
}
