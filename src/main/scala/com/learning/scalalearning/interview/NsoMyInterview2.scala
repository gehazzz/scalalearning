package com.learning.scalalearning.interview

object NsoMyInterview2 extends App {
  def factorial(n: Int): Int = {
    if (n <= 1){
      println(1)
      1
    }
    else {
      val result = n * factorial(n - 1)
      println(result)
      result
    }
  }
  factorial(5)

  def pow (base: Int, power: Int):Int = {
    if (power == 1)
      base
    else
      base*pow(base, power - 1)
  }
  println(pow(2,2))
}
