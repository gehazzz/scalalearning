package com.learning.scalalearning.interview

object NSO extends App {
  def head[T](list: List[T]): Option[T] = {
    list match {
      case List() => None
      case a :: _ => Some(a)
    }
  }

  val list = List(1, 2, 3)
  val empty = List()
  println(head(list))
  println(head(empty))

  def callAllPerfectNumbersUpTo(n: Int): List[Int] = {
    (1 to n)
      .map(i => {
        val dividers = (1 to i / 2).filter(m => i % m == 0)
        (i, dividers)
      })
      .filter(t => t._1 == t._2.sum)
      .map(t => t._1).toList
  }

  def perfect(n: Int): List[Int] = {
    (1 to n).filter(n => n % 9 == 1).toList
  }

  println("perfect numbers")
  println(perfect(100))

  def isConnected(from: Int, to: Int, g: Map[Int, List[Int]], viewed: Set[Int]): Boolean = {
    if(viewed.contains(from)) {
      return false
    }

    val l = viewed.+(from)
    true
  }
}
