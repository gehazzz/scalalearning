package com.learning.scalalearning.interview

object NsoTasks extends App {

  println("\n==== Task 1 ====")
  println(s"head(List(1, 2, 3)) = ${Task1.head(List(1, 2, 3))}")
  println(s"head(List()) = ${Task1.head(List())}")

  println("\n==== Task 2 ====")
  println(s"all perfect number up to 30 are ${Task2.calAllPerfectNumbersUpTo(30).mkString(" and ")}")
  println(s"all perfect number up to 500 are ${Task2.calAllPerfectNumbersUpTo(500).mkString(" and ")}")


  val graph = Map(
    1 -> List(2, 3, 4),
    2 -> List(3),
    4 -> List(3, 1, 8),
    5 -> List(6),
    10 -> List(7, 1),
    8 -> List(15)
  )

  println("\n==== Task 3 ====")
  println(s"You have graph ${graph}")
  println(s"Are 4 and 2 connected? Answer: ${Task3.isConnected(4, 2, graph, Set())}")
  //println(s"Are 10 and 15 connected? Answer: ${Task3.isConnected(10, 15, graph, Set())}")
  //println(s"Are 1 and 5 connected? Answer: ${Task3.isConnected(1, 5, graph, Set())}")


}

object Task1 {
  def head[T](list: List[T]): Option[T] = {
    list match {
      case List() => None
      case a :: _ => Some(a)
    }
  }
}

object Task2 {
  def calAllPerfectNumbersUpTo(n: Int): List[Int] = {
    (1 to n)
      .filter(isPerfect)
      .toList
  }

  //noinspection SimplifiableFoldOrReduce
  def isPerfect(n: Int): Boolean = {
    n == (1 to n / 2)
      .filter(m => n % m == 0)
      .reduceOption(_ + _)
      .getOrElse(0)
  }
}

object Task3 {
  def isConnected(from: Int, to: Int, g: Map[Int, List[Int]], viewed: Set[Int]): Boolean = {
    if (viewed.contains(from)) {
      return false
    }
    val l = viewed + from

    if (g.getOrElse(from, List()).contains(to)) {
      true
    } else {
      g.getOrElse(from, List())
        .exists(isConnected(_, to, g, l))
    }
  }
}




