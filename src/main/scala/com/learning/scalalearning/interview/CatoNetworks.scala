package com.learning.scalalearning.interview

object CatoNetworks {
  //hof
  def nTimes(f: Int => Int, n:Int): Int => Int = {
    if (n <= 0) (x: Int) => x
    else nTimes(f, n-1)
  }

  def fun(f:Int => Int): Int => Int = {
    (x: Int) => f(5) + 4
  }

  def fun2(f:Int => Int): Int => Int = (x: Int) => f(5) + 4

  //Currying
  def f(m: Int)(n: Int):Int = m*n
  val f3 = f(3) _
  val result = f3(4)

  //partially applied
  def ff(m:Int, n:Int):Int = m*n
  val ff3 = ff(3, _)
  val resultff = ff3(4)
}
