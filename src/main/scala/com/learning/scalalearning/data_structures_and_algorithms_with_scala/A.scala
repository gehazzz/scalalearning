package com.learning.scalalearning.data_structures_and_algorithms_with_scala


import scala.io.Source
//import scala.util.{Failure, Success, Try, Using}
//works with <scala.version>2.13.1</scala.version>
object A extends App {

/*  case class ListingDetails(id: Long,
                            street: String,
                            status: String,
                            price: Long,
                            bedrooms: Int,
                            bathrooms: Int,
                            sq_ft: Int,
                            lat: Double,
                            lng: Double)
  //https://stackoverflow.com/questions/4458864/whats-the-right-way-to-use-scala-io-source
  //Source.fromFile("C:\\Users\\Gena\\IdeaProjects\\scalalearning\\src\\main\\resources\\listing-details.csv").getLines.toList
  //val res: Try[List[String]] = Using(Source.fromFile("C:\\Users\\Gena\\IdeaProjects\\scalalearning\\src\\main\\resources\\listing-details.csv")){ source => source.getLines().toList}
  val res: Try[List[String]] = Using(Source.fromURL("https://server-assignment.s3.amazonaws.com/listing-details.csv")) { source => source.getLines().toList }
  res match {
    case Success(rows) => parse(rows)
    case Failure(exception) => {
      println(exception)
      List.empty
    }
  }

  def parse(rows: List[String]): List[ListingDetails] = {
    rows.tail.map {
      case s"$id,$street,$status,$price,$bedrooms,$bathrooms,$sq_ft,$lat,$lng" => ListingDetails(id.toLong,
        street.toString,
        status.toString,
        price.toLong,
        bedrooms.toInt,
        bathrooms.toInt,
        sq_ft.toInt,
        lat.toDouble,
        lng.toDouble)
      case line          => throw new RuntimeException(s"Cannot read '$line'")
    }
  }*/
}
