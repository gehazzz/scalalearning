package com.learning.scalalearning.data_structures_and_algorithms_with_scala.foundational_components

object ListsAndVectors extends App {
  val myFruitsLinkedList = List("grape", "banana", "apple", "mango")

  val myFruitsVector = Vector("grape", "banana", "apple", "mango")

  myFruitsVector(3)

  val yourFruits = Vector("cucumber", "tomato")

  val combinedFruits = myFruitsVector ++ yourFruits

  val notQuiteFruits = combinedFruits.filter(x => x == "tomato")
}
