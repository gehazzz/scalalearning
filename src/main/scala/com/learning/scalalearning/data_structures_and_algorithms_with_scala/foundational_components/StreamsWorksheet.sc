import scala.annotation.tailrec
import scala.collection.immutable.Stream.cons

//In Section 1.3(AppliedTechniquesForEfficientComputation.scala), we implemented both lazy evaluation and memoization. Fortunately,
//we don’t have to implement those manually if we are using Scala’s Stream, which
//is lazy, memoized, and immutable. This allows us to create infinite sequences using
//Stream. Now, let’s look at Stream in action.
val myStream = Stream("message1", "message2", "message3")
//Even though we supplied three messages, only the first is listed, which means only
//the first element of the stream was computed. It is a lazy computation. Now, if we
//access an element at an index other than 0, it will force computation. Let’s access
//the second element.
myStream(1)
myStream
//It is evident that it has computed the second element as well. Also it is clear that it
//only computed until second element. So this is truly lazy computation. Now let’s try
//a stream with a few more elements. For brevity, we use numbers.
val myNums = Stream(0, 1, 2, 3, 4, 5, 6, 7, 8, 9)
//we access 3 element but stream will calculate all elements before 3 element
myNums(3)
myNums
myNums(9)
myNums
//result
// res2: Int = 3
//res3: scala.collection.immutable.Stream[Int] = Stream(0, 1, 2, 3, ?)
//res4: Int = 9
//res5: scala.collection.immutable.Stream[Int] = Stream(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, ?)

//When we accessed the element at index 3, it
//automatically computed all elements until index 3. This is true when we accessed
//an element at index 9. Streams can also be create using the # :: operator. Let’s see
//an example.
val myStreamChars = 'a' #:: 'b' #:: 'c' #:: 'd' #:: Stream.empty
myStreamChars

//So we can see that Stream behaves the same way. empty is an empty Stream. Let’s
//look at one more way of creating a stream, using the cons function.
val myTestSteam = Stream.cons('a', Stream.empty)
//Infinite sequences are common in mathematics. They solve some of the most demanding
//computing problems like handling continuously generated log files, tweets,
//etc. So let’s create one sample infinite stream.
def createInfStream(x: Int): Stream[Int] = {
  println("Processing...")
  cons(x, createInfStream(x + 1))
}
//We created a function createIn f Stream that creates an infinite stream using cons.
//It looks line an infinite loop, which is true. But the lazy nature of Stream makes it
//computable, as it is on-demand computing. Now, let’s access elements of the stream.
val myInfIntStream = createInfStream(0)
myInfIntStream(0)
myInfIntStream(1)
myInfIntStream(3)
myInfIntStream(5)
myInfIntStream(5)
myInfIntStream(6)
myInfIntStream(6)
//We can see that the computation is on-demand. When we invoked createIn f Stream,
//it created a stream with the first element. We accessed index 6 twice; the first time
//we accessed it, it was computed. When we accessed it a second time, it reused the
//pre-computed value. So it is lazy and memoized. Now, we need to try one more
//thing: immutability. Let’s look at the following code snippet.
var testImmInfStr = createInfStream(5)
testImmInfStr(4)
testImmInfStr(6)
testImmInfStr(5)
testImmInfStr(0)
testImmInfStr(3)
testImmInfStr(5)
testImmInfStr(4)
testImmInfStr(4)
//testImmInfStr(5) = 15 - will cause error: error: value update is not a member of
//Stream[Int]
//testImmInfStr(5) = 15
//ˆ
//Our starting value is 5. Then we accessed the element at index 4, which forced
//computation until index 4. That was true for index 6 as well, except it had to compute
//only two values, the remainder being reused. Next, when we accessed the element at
//index 5, it got a pre-calculated value; this was true for index 0 and index 3. Finally,
//we tried to assign a new value at index 5. Since Stream is immutable, it refused our
//request to update.

//Now, let’s quickly look at Stream to List conversion. In the case of infinite
//streams, we are trying to convert an infinite steam to a list. In this case, before
//converting, it tries to evaluate all of them, so evaluation never ends as it is an infinite
//list. So we need to be careful during stream to list conversion. There is a solution
//for this problem. Let’s look at the code snippet below.
testImmInfStr.take(5).toList
//testImmInfStr.toList // will never finishes

//The first one terminates but the second one doesn’t. What is the reason? In the first
//conversion, we limited the number of elements in the stream, which limited the evaluation
//and the computation terminated. In the second case, we gave an infinite set
//of numbers, so it kept evaluating those numbers first. Evaluation completes before
//conversion, which is the thing that we need to be careful about while converting a
//stream to a list.


//Lastly, as an infinite series example, let’s compute the Fibonacci series starting
//from 0.We define the function createFiboSeries, which takes the first two Fibonacci
//numbers. It is a recursive computation, so every time we iterate, the current second
//number becomes the new first number and the next number in the Fibonacci series is
//the sum of the current two numbers. The cons function prepends the first parameter,
//which is a single value in this case, to the second parameter, which is a stream,
//because createFiboSeries returns a stream of integers.
def createFiboSeries(a: Int, b: Int): Stream[Int] = {
  cons(a, createFiboSeries(b, a+b))
}
val myFiboSeries = createFiboSeries(0, 1)
myFiboSeries.take(7).foreach(println)