val myNumbers = Array(0, 2, 4, 6, 8, 10)
println(myNumbers(5))
val numMult2 = myNumbers.map(_ * 2)
numMult2.foreach(i => print(s"$i "))
println()

def multBy2(myArray: Array[Int]): Array[Int] = {
  val size = myArray.length
  val myTempArray = new Array[Int](size)
  var i = 0
  while(i < size) {
    myTempArray(i) = myArray(i) * 2
    i += 1
  }
  myTempArray
}
multBy2(myNumbers).foreach(i => print(s"$i "))

//revers
println(myNumbers.reverse)

//Two arrays can be combined using the ++ operator as shown below.
val array1 = Array(1,2,3,4,5)
val array2 = Array(6,7,8,9,10)
println(array1 ++ array2)

val fruits = Array("banana", "apple","orange")
fruits.sorted
//the same as contains bur gets function as parameter and not value
val isFruitExist = fruits.exists(x => x == "banana")

val myNums = Array(1, 2, 2, 3, 4, 5, 6, 7, 7, 8, 9, 5, 9, 10)

myNums.filter(x => x == 2) // result = Array(2, 2)
myNums.filter(x => x % 2 == 0)

//returns first occurrence
myNums.find(x => x % 2 == 0)// result Option[Int] = Some(2)