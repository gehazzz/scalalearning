
val myFruitsLinkedList = List("grape", "banana", "apple", "mango")

val myFruitsVector = Vector("grape", "banana", "apple", "mango")

myFruitsVector(3)


val yourFruits = Vector("cucumber", "tomato")

val combinedFruits = myFruitsVector ++ yourFruits

val notQuiteFruits = combinedFruits.filter(x => x == "tomato")

val myWords = List("dog", "cat", "rat", "goat", "horse")

myWords groupBy {x => x.length}