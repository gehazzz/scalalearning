package com.learning.scalalearning.data_structures_and_algorithms_with_scala.foundational_components

import java.util.Calendar

object AppliedTechniquesForEfficientComputation extends App {
  //lazy
  lazy val lazyTime = Calendar.getInstance.getTime

  val eagerTime = Calendar.getInstance.getTime

  //When myMethod is called, the integer argument is not evaluated. It is evaluated at
  //the time of printing because that is when it is needed.
  def myMethod(myArg: () => Int) = println(myArg())

  myMethod(() => 5)
  //Now, the main question is what do we gain by making evaluations lazy? In the code
  //snippet above, we have smaller values to hold in the memory. But imagine thousands
  //of records being evaluated. If we are not using those records then holding them in the
  //computer’s memory is wasteful.

  //Memoization
  //Now, let’s walk through two implementations of factorial calculation—one
  //without memoization and another with memoization.
  def calcFactorial(x: Int): Int = {
    if (x == 0 || x == 1)
      1
    else {
      println("Computing factorial")
      x * calcFactorial(x - 1)
    }
  }
  //The code snippet above is a factorial calculation without memoization. It performs
  //computation in every step, which can be seen in the following console output. Also,
  //if we run calcFactorial(5) again we get the same console output, which confirms
  //the re-computation.

  //Now, let’s implement the same with memoization. In the code snippet below, we
  //implement caching so that calculations performed earlier can be reused.
  class FactorialMemoiz {
    var cache: Map[Int, Int] = Map()

    def lookup(num: Int): Int = cache.getOrElse(num, 0)

    def calcFactMemoiz(x: Int): Int = {
      if (x == 0 || x == 1)
        1
      else if (lookup(x) > 0) {
        println("Performing lookup")
        lookup(x)
      } else {
        println("Performing calculation")
        val factorial = x * calcFactMemoiz((x - 1))
        cache += x -> factorial
        factorial
      }
    }
  }

  val factMem = new FactorialMemoiz()
  println(factMem.calcFactMemoiz(3))
  println(factMem.calcFactMemoiz(5))

}
