package com.learning.scalalearning.data_structures_and_algorithms_with_scala.foundational_components

object Streams extends App {
  //In Section 1.3(AppliedTechniquesForEfficientComputation.scala), we implemented both lazy evaluation and memoization. Fortunately,
  //we don’t have to implement those manually if we are using Scala’s Stream, which
  //is lazy, memoized, and immutable. This allows us to create infinite sequences using
  //Stream. Now, let’s look at Stream in action.
  val myStream = Stream("message1", "message2", "message3")
  //Even though we supplied three messages, only the first is listed, which means only
  //the first element of the stream was computed. It is a lazy computation. Now, if we
  //access an element at an index other than 0, it will force computation. Let’s access
  //the second element.
  myStream(1)
}
